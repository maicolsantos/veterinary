import Backbone from 'backbone'

const SingleModel = Backbone.Model.extend()

export default SingleModel
