import Backbone from 'backbone'
import _ from 'underscore'

import homeView from './view/HomeView'
import singleView from './view/SingleView'

const AppRouter = Backbone.Router.extend({
  routes: {
    '': 'home',
    ':id': 'single',
  },
})

const initialize = () => {
  const appRouter = new AppRouter

  appRouter.on('route:home', () => {
    homeView.render()
  })
  appRouter.on('route:single', id => {
    singleView.render(id)
  })

  Backbone.history.start()
}

initialize()
