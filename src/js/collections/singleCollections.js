import Backbone from 'backbone'

import SingleModel from '../models/singleModels'

const SingleCollection = Backbone.Collection.extend({
  model: SingleModel,
  url: process.env.URL_API,
})

export default SingleCollection
