import Backbone from 'backbone'

import HomeModel from '../models/homeModels'

const HomeCollection = Backbone.Collection.extend({
  model: HomeModel,
  url: process.env.URL_API,
})

export default HomeCollection
