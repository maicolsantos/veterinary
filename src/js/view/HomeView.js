import $ from 'jquery'
import Backbone from 'backbone'
import _ from 'underscore'

import HomeCollection from '../collections/homeCollections'

const HomeView = Backbone.View.extend({
  el: document.getElementById('app'),
  template: _.template(document.getElementById('home-view').innerHTML),
  initialize () {
    this.homeCollection = new HomeCollection
  },
  render (term = this.homeCollection) {
    this.$el.empty()
    this.$el.html(_.template(document.getElementById('filter-home-view').innerHTML))
    this.renderList(term)
  },
  events: {
    'keyup input[type=text]': 'search',
  },
  search () {
    const value = document.getElementById('search').value
    const terms = term => (`
      ${term.name}
      ${term.details.address}
      ${term.details.phone}
      ${term.details.email}
    `).normalize('NFD').replace(/[^\w\s]+/g, '').toLowerCase()

    const result = this.result.responseJSON.filter(term => (
      terms(term).indexOf(
        value.normalize('NFD').replace(/[^\w\s]+/g, '').toLowerCase()) >= 0
      )
    )

    this.appendList(result)
  },
  renderList (result) {
    this.result = result.fetch({
      success: data => this.appendList(data.models.map(i => i.attributes)),
      error: e => console.error('error', e),
    })
  },
  appendList (lists) {
    $('.list').remove()
    lists.map(list => this.$el.append(this.template({ ...list })))
  }
})

export default new HomeView
