import Backbone from 'backbone'
import _ from 'underscore'

import SingleCollection from '../collections/singleCollections'

const SingleView = Backbone.View.extend({
  el: document.getElementById('app'),
  template: _.template(document.getElementById('single-view').innerHTML),
  initialize () {
    this.SingleCollection = new SingleCollection
  },
  render (id) {
    this.$el.empty()
    this.SingleCollection.set({ id }).fetch({
      success: data => this.$el.append(this.template(data.attributes)),
      error: e => console.error(e),
    })
  }
})

export default new SingleView
