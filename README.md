# Veterinary Clinic

## Install
```
yarn
```

## Run server
```
yarn server
```

## Run project
```
yarn start
```

Server running at [http://localhost:1234](http://localhost:1234)

O projeto usa `sass`para renderizar os estilos, caso não tenha o sass instalado [clique aqui](https://sass-lang.com/install)
